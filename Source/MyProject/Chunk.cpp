// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "Chunk.h"

const int32 bTriangles[] = { 2, 1, 0, 0, 3, 2 };
const FVector2D bUVs[] = { FVector2D(0.0f, 0.0f), FVector2D(0.0f, 1.0f) , FVector2D(1.0f, 1.0f) , FVector2D(1.0f, 0.0f) };
const FVector bNormals0[] = { FVector(0.0f, 0.0f, 1.0f), FVector(0.0f, 0.0f, 1.0f) , FVector(0.0f, 0.0f, 1.0f) , FVector(0.0f, 0.0f, 1.0f) };
const FVector bNormals1[] = { FVector(0.0f, 0.0f, -1.0f), FVector(0.0f, 0.0f, -1.0f) , FVector(0.0f, 0.0f, -1.0f) , FVector(0.0f, 0.0f, -1.0f) };
const FVector bNormals2[] = { FVector(0.0f, 1.0f, 0.0f), FVector(0.0f, 1.0f, 0.0f) , FVector(0.0f, 1.0f, 0.0f) , FVector(0.0f, 1.0f, 0.0f) };
const FVector bNormals3[] = { FVector(0.0f, -1.0f, 0.0f), FVector(0.0f, -1.0f, 0.0f) , FVector(0.0f, -1.0f, 0.0f) , FVector(0.0f, -1.0f, 0.0f) };
const FVector bNormals4[] = { FVector(1.0f, 0.0f, 0.0f), FVector(1.0f, 0.0f, 0.0f) , FVector(1.0f, 0.0f, 0.0f) , FVector(1.0f, 0.0f, 0.0f) };
const FVector bNormals5[] = { FVector(-1.0f, 0.0f, 0.0f), FVector(-1.0f, 0.0f, 0.0f) , FVector(-1.0f, 0.0f, 0.0f) , FVector(-1.0f, 0.0f, 0.0f) };
const FVector bMask[] = { FVector(0.0f, 0.0f, 1.0f), FVector(0.0f, 0.0f, -1.0f), FVector(0.0f, 1.0f, 0.0f), FVector(0.0f, -1.0f, 0.0f), FVector(1.0f, 0.0f, 0.0f), FVector(-1.0f, 0.0f, 0.0f) };


const int32 ChunkXLength = 16;
const int32 ChunkYLength = 16;
const int32 ChunkZLength = 30;

const int32 BlockSize = 100;
const int32 BlockSizeHalf = 50;

// Sets default values
AChunk::AChunk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SceneComp = CreateDefaultSubobject<USceneComponent>(TEXT("ChunkRoot"));
	RootComponent = SceneComp;

	UMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("BlockRock"));
	UMesh->AttachToComponent(SceneComp, FAttachmentTransformRules::KeepWorldTransform);

	static ConstructorHelpers::FObjectFinder<UMaterial> Material_Something(TEXT("Material'/Game/StarterContent/Materials/GrassBlock_M.GrassBlock_M'"));
	if (Material_Something.Succeeded()) {
		MaterialGrass = Material_Something.Object;
		materials.Add(MaterialGrass);
	}

	static ConstructorHelpers::FObjectFinder<UMaterial> Material_Something1(TEXT("Material'/Game/StarterContent/Materials/M_Concrete_Grime.M_Concrete_Grime'"));
	if (Material_Something1.Succeeded()) {
		MaterialRock = Material_Something1.Object;
		materials.Add(MaterialRock);
	}
	
}

// Called when the game starts or when spawned
void AChunk::BeginPlay()
{
	GenerateNoise();
	RenderChunk();
	Super::BeginPlay();
	
}

// Called every frame
void AChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AChunk::GenerateNoise()
{
	int32 ChunkXIndex = 0;
	int32 ChunkYIndex = 0;
	int32 ChunkElements = 16;
		
	float xMult = 0.05f;
	float yMult = 0.05f;
	float freqHead = 5.0f;

	FString SaveDirectory = FString("F:/Savegame");
	FString FileName = FString("Noise.txt");
	FString TextToRead;
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	if (PlatformFile.DirectoryExists(*SaveDirectory)) {
		FString AbsoluteFilePath = SaveDirectory + "/" + FileName;

		if (PlatformFile.FileExists(*AbsoluteFilePath)) {
			FFileHelper::LoadFileToString(TextToRead, *AbsoluteFilePath);
			SimplexNoiseS::setNoiseSeed(FCString::Atoi(*TextToRead));
		}
		else
		{
			SimplexNoiseS::setNoiseSeed(234523);
		}
	} else {
		SimplexNoiseS::setNoiseSeed(234523);
	}
	

	FRotator SimpleRotation(0.0f, 0.0f, 0.0f);

	int32 octaveWeight1 = 4;
	int32 octaveWeight2 = 8;
	int32 octaveWeight3 = 16;
	int32 octaveWeight4 = 4;

	float octaveMult1 = 0.01f;
	float octaveMult2 = 0.01f;
	float octaveMult3 = 0.004f;
	float octaveMult4 = 0.05f;

	for (int NY = (GetActorLocation().Y / 100); NY < (GetActorLocation().Y/100) + ChunkYLength; NY++) {
		for (int NX = (GetActorLocation().X / 100); NX < (GetActorLocation().X/100) + ChunkXLength; NX++) {
			float noiseOctave = 0.0f;

			noiseOctave += SimplexNoiseS::SimplexNoise2D(((ChunkXIndex * ChunkElements) + NX) * octaveMult1, ((ChunkYIndex * ChunkElements) + NY) * octaveMult1) * octaveWeight1;
			noiseOctave += SimplexNoiseS::SimplexNoise2D(((ChunkXIndex * ChunkElements) + NX) * octaveMult2, ((ChunkYIndex * ChunkElements) + NY) * octaveMult2) * octaveWeight2;
			noiseOctave += SimplexNoiseS::SimplexNoise2D(((ChunkXIndex * ChunkElements) + NX) * octaveMult3, ((ChunkYIndex * ChunkElements) + NY) * octaveMult3) * octaveWeight3;
			noiseOctave += FMath::Clamp(SimplexNoiseS::SimplexNoise2D(((ChunkXIndex * ChunkElements) + NX) * octaveMult4, ((ChunkYIndex * ChunkElements) + NY) * octaveMult4) * octaveWeight4, 0.0f, 5.0f);

			noise.Add(noiseOctave);
		}
	}

	FString chunkBlockString;

	for (int32 x = 0; x < ChunkXLength; x++) {
		for (int32 y = 0; y < ChunkYLength; y++) {
			for (int32 z = 0; z < ChunkZLength; z++) {
				int32 chunkBlockVal = 0;
				int32 currentNoiseVal = noise[x + y * ChunkXLength];

				if (z == 21 + currentNoiseVal) {
					chunkBlockVal = 1;
				} else if (z < 21 + currentNoiseVal) {
					chunkBlockVal = 2;
				} else {
					chunkBlockVal = 0;
				}

				chunkBlockString.Append(FString::FromInt(noise[x + y * ChunkXLength]));
				chunkFields.Add(chunkBlockVal);
			}
		}
	}

	if (GEngine)
		GEngine->AddOnScreenDebugMessage(1, 10.0f, FColor::Blue, chunkBlockString);
}

void AChunk::RenderChunk()
{
	TArray<IMeshSection> iMeshSections;
	iMeshSections.SetNum(materials.Num());
	int32 el_num = 0;


	FString SaveDirectory = FString("F:/Savegame");
	FString FileName = FString("MyBlocks.txt");
	FString TextToSave;
	bool AllowOverwriting = false;
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();




	for (int32 z = 0; z < ChunkZLength; z++) {
		for (int32 y = 0; y < ChunkYLength; y++) {
			for (int32 x = 0; x < ChunkXLength; x++) {
				int32 someIndex = (x * ChunkZLength * ChunkYLength) + (y * ChunkZLength) + z;
				int32 chunkBlockValue = chunkFields[someIndex];

				TextToSave += "X: " + FString::FromInt(x) + ", Y: " + FString::FromInt(y) + ", Z: " + FString::FromInt(z) + ", V: " + FString::FromInt(chunkBlockValue) + "\n";

				if (chunkBlockValue > 0) {
					chunkBlockValue--;

					TArray<FVector> &Vertices = iMeshSections[chunkBlockValue].Vertices;
					TArray<int32> &Triangles = iMeshSections[chunkBlockValue].Triangles;
					TArray<FVector> &Normals = iMeshSections[chunkBlockValue].Normals;
					TArray<FVector2D> &UVs = iMeshSections[chunkBlockValue].UVs;
					TArray<FLinearColor> &VertexColors = iMeshSections[chunkBlockValue].VertexColors;
					TArray<FProcMeshTangent> &Tangents = iMeshSections[chunkBlockValue].Tangents;
					int32 elementID = iMeshSections[chunkBlockValue].elementID;


					int triangle_num = 0;

					for (int i = 0; i < 6; i++) {

						Triangles.Add(bTriangles[0] + triangle_num + elementID);
						Triangles.Add(bTriangles[1] + triangle_num + elementID);
						Triangles.Add(bTriangles[2] + triangle_num + elementID);
						Triangles.Add(bTriangles[3] + triangle_num + elementID);
						Triangles.Add(bTriangles[4] + triangle_num + elementID);
						Triangles.Add(bTriangles[5] + triangle_num + elementID);
						triangle_num += 4;

						switch (i)
						{
						case 0: {
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));

							Normals.Append(bNormals0, ARRAY_COUNT(bNormals0));
							break;
						}

						case 1: {
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));

							Normals.Append(bNormals1, ARRAY_COUNT(bNormals1));
							break;
						}

						case 2: {
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));

							Normals.Append(bNormals2, ARRAY_COUNT(bNormals2));
							break;
						}

						case 3: {
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));

							Normals.Append(bNormals3, ARRAY_COUNT(bNormals3));
							break;
						}

						case 4: {
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));

							Normals.Append(bNormals5, ARRAY_COUNT(bNormals4));
							break;
						}

						case 5: {
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), -BlockSizeHalf + (z * BlockSize)));
							Vertices.Add(FVector(-BlockSizeHalf + (x * BlockSize), -BlockSizeHalf + (y * BlockSize), BlockSizeHalf + (z * BlockSize)));

							Normals.Append(bNormals4, ARRAY_COUNT(bNormals5));
							break;
						}
						}

						UVs.Append(bUVs, ARRAY_COUNT(bUVs));

						FColor color2 = FColor(255, 255, 255, i);
						VertexColors.Add(color2);
						VertexColors.Add(color2);
						VertexColors.Add(color2);
						VertexColors.Add(color2);
					}

					el_num += triangle_num;
					iMeshSections[chunkBlockValue].elementID += triangle_num;
				}
			}
		}
	}

	UMesh->ClearAllMeshSections();

	for (int i = 0; i < iMeshSections.Num(); i++) {
		if(iMeshSections[i].Vertices.Num() > 0)
			UMesh->CreateMeshSection_LinearColor(i, iMeshSections[i].Vertices, iMeshSections[i].Triangles, iMeshSections[i].Normals, iMeshSections[i].UVs, iMeshSections[i].VertexColors, iMeshSections[i].Tangents, true);
	}

	int32 s = 0;
	while (s < materials.Num()) {
		UMesh->SetMaterial(s, materials[s]);
		s++;
	}


	if (PlatformFile.CreateDirectoryTree(*SaveDirectory)) {
		FString AbsoluteFilePath = SaveDirectory + "/" + FileName;

		if (AllowOverwriting || !PlatformFile.FileExists(*AbsoluteFilePath)) {
			FFileHelper::SaveStringToFile(TextToSave, *AbsoluteFilePath);
		}
	}
}

