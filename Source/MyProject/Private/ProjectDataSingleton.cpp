// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "ProjectDataSingleton.h"

UProjectDataSingleton::UProjectDataSingleton(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	chunkXSize = 16;
	chunkYSize = 16;
	chunkZSize = 256;
}


