// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "BlockGrass.h"


// Sets default values
ABlockGrass::ABlockGrass()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	UStaticMeshComponent* Voxel = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BlockRock"));
	RootComponent = Voxel;

	static ConstructorHelpers::FObjectFinder<UStaticMesh> StaticMeshRef(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Cube_Sand.Shape_Cube_Sand'"));
	if (StaticMeshRef.Succeeded()) {
		Voxel->SetStaticMesh(StaticMeshRef.Object);
		Voxel->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
	}

}

// Called when the game starts or when spawned
void ABlockGrass::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABlockGrass::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

