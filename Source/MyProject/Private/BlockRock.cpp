// Fill out your copyright notice in the Description page of Project Settings.

#include "MyProject.h"
#include "BlockRock.h"

const int32 bTriangles[] = { 2, 1, 0, 0, 3, 2 };
const FVector2D bUVs[] = { FVector2D(0.0f, 0.0f), FVector2D(0.0f, 1.0f) , FVector2D(1.0f, 1.0f) , FVector2D(1.0f, 0.0f) };
const FVector bNormals0[] = { FVector(0.0f, 0.0f, 1.0f), FVector(0.0f, 0.0f, 1.0f) , FVector(0.0f, 0.0f, 1.0f) , FVector(0.0f, 0.0f, 1.0f) };
const FVector bNormals1[] = { FVector(0.0f, 0.0f, -1.0f), FVector(0.0f, 0.0f, -1.0f) , FVector(0.0f, 0.0f, -1.0f) , FVector(0.0f, 0.0f, -1.0f) };
const FVector bNormals2[] = { FVector(0.0f, 1.0f, 0.0f), FVector(0.0f, 1.0f, 0.0f) , FVector(0.0f, 1.0f, 0.0f) , FVector(0.0f, 1.0f, 0.0f) };
const FVector bNormals3[] = { FVector(0.0f, -1.0f, 0.0f), FVector(0.0f, -1.0f, 0.0f) , FVector(0.0f, -1.0f, 0.0f) , FVector(0.0f, -1.0f, 0.0f) };
const FVector bNormals4[] = { FVector(1.0f, 0.0f, 0.0f), FVector(1.0f, 0.0f, 0.0f) , FVector(1.0f, 0.0f, 0.0f) , FVector(1.0f, 0.0f, 0.0f) };
const FVector bNormals5[] = { FVector(-1.0f, 0.0f, 0.0f), FVector(-1.0f, 0.0f, 0.0f) , FVector(-1.0f, 0.0f, 0.0f) , FVector(-1.0f, 0.0f, 0.0f) };
const FVector bMask[] = { FVector(0.0f, 0.0f, 1.0f), FVector(0.0f, 0.0f, -1.0f), FVector(0.0f, 1.0f, 0.0f), FVector(0.0f, -1.0f, 0.0f), FVector(1.0f, 0.0f, 0.0f), FVector(-1.0f, 0.0f, 0.0f) };


// Sets default values
ABlockRock::ABlockRock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	UMesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("BlockRock"));
	RootComponent = UMesh;

	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FColor> VertexColors;
	TArray<FProcMeshTangent> Tangents;


	float voxelHalf = 50;
	float voxelWhole = 100;

	int triangle_num = 0;

	for (int i = 0; i < 6; i++) {
		Triangles.Add(bTriangles[0] + triangle_num);
		Triangles.Add(bTriangles[1] + triangle_num);
		Triangles.Add(bTriangles[2] + triangle_num);
		Triangles.Add(bTriangles[3] + triangle_num);
		Triangles.Add(bTriangles[4] + triangle_num);
		Triangles.Add(bTriangles[5] + triangle_num);
		triangle_num += 4;

		switch (i)
		{
		case 0: {
			Vertices.Add(FVector(-voxelHalf + voxelWhole, voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, -voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, -voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, voxelHalf + voxelWhole, voxelHalf + voxelWhole));

			Normals.Append(bNormals0, ARRAY_COUNT(bNormals0));
			break;
		}

		case 1: {
			Vertices.Add(FVector(voxelHalf + voxelWhole, -voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, -voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, voxelHalf + voxelWhole, -voxelHalf + voxelWhole));

			Normals.Append(bNormals1, ARRAY_COUNT(bNormals1));
			break;
		}

		case 2: {
			Vertices.Add(FVector(voxelHalf + voxelWhole, voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, voxelHalf + voxelWhole, voxelHalf + voxelWhole));

			Normals.Append(bNormals2, ARRAY_COUNT(bNormals2));
			break;
		}

		case 3: {
			Vertices.Add(FVector(-voxelHalf + voxelWhole, -voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, -voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, -voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, -voxelHalf + voxelWhole, voxelHalf + voxelWhole));

			Normals.Append(bNormals3, ARRAY_COUNT(bNormals3));
			break;
		}

		case 4: {
			Vertices.Add(FVector(voxelHalf + voxelWhole, -voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, -voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(voxelHalf + voxelWhole, voxelHalf + voxelWhole, voxelHalf + voxelWhole));

			Normals.Append(bNormals5, ARRAY_COUNT(bNormals4));
			break;
		}

		case 5: {
			Vertices.Add(FVector(-voxelHalf + voxelWhole, voxelHalf + voxelWhole, voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, -voxelHalf + voxelWhole, -voxelHalf + voxelWhole));
			Vertices.Add(FVector(-voxelHalf + voxelWhole, -voxelHalf + voxelWhole, voxelHalf + voxelWhole));

			Normals.Append(bNormals4, ARRAY_COUNT(bNormals5));
			break;
		}
		}

		UVs.Append(bUVs, ARRAY_COUNT(bUVs));

		FColor color2 = FColor(0, 0, 255);
		VertexColors.Add(color2);
		VertexColors.Add(color2);
		VertexColors.Add(color2);
		VertexColors.Add(color2);
	}

	UMesh->ClearAllMeshSections();
	UMesh->CreateMeshSection(0, Vertices, Triangles, Normals, UVs, VertexColors, Tangents, false);

	
}

// Called when the game starts or when spawned
void ABlockRock::BeginPlay()
{
	/*static ConstructorHelpers::FObjectFinder<UMaterial> Material_Something(materialName);
	if (Material_Something.Succeeded())
		UMesh->SetMaterial(0, Material_Something.Object);*/

	

	Super::BeginPlay();
}

// Called every frame
void ABlockRock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

