// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.CameraComponent	

#include "MyProject.h"
#include "MyProjectCharacter.h"
#include "MyProjectProjectile.h"
#include "Animation/AnimInstance.h"
#include "GameFramework/InputSettings.h"
#include "Kismet/HeadMountedDisplayFunctionLibrary.h"
#include "MotionControllerComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// AMyProjectCharacter

AMyProjectCharacter::AMyProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	//  Chunk Variables Setup
	Rotation = FRotator(0.0f, 0.0f, 0.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->RelativeLocation = FVector(-39.56f, 1.75f, 64.f); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->RelativeRotation = FRotator(1.9f, -19.19f, 5.2f);
	Mesh1P->RelativeLocation = FVector(-0.5f, -4.4f, -155.7f);

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->Hand = EControllerHand::Right;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(true);			// only the owning player will see this mesh
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	static ConstructorHelpers::FObjectFinder<UClass> ChunkClassFinder(TEXT("Class'/Script/MyProject.Chunk'"));
	if (ChunkClassFinder.Succeeded()) {
		ChunkClass = ChunkClassFinder.Object;
	}

	/*static ConstructorHelpers::FObjectFinder<UClass> BlockRockClassFinder(TEXT("Class'/Script/MyProject.BlockRock'"));
	if (BlockRockClassFinder.Succeeded()) {
		BlockRockClass = BlockRockClassFinder.Object;
	}

	static ConstructorHelpers::FObjectFinder<UClass> BlockGrassClassFinder(TEXT("Class'/Script/MyProject.BlockGrass'"));
	if (BlockGrassClassFinder.Succeeded()) {
		BlockGrassClass = BlockGrassClassFinder.Object;
	}*/

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;
}

void AMyProjectCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}

	GenerateWorld();
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMyProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	//InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMyProjectCharacter::TouchStarted);
	if (EnableTouchscreenMovement(PlayerInputComponent) == false)
	{
		PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &AMyProjectCharacter::BlockCreate);
		PlayerInputComponent->BindAction("DestroyBlock", IE_Pressed, this, &AMyProjectCharacter::BlockDestroy);
	}

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &AMyProjectCharacter::OnResetVR);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMyProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMyProjectCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMyProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMyProjectCharacter::LookUpAtRate);
}

void AMyProjectCharacter::GenerateWorld()
{	
	FString SaveDirectory = FString("F:/Savegame");
	FString FileName = FString("Noise.txt");
	FString TextToSave;
	bool AllowOverwriting = true;
	IPlatformFile& PlatformFile = FPlatformFileManager::Get().GetPlatformFile();

	if (PlatformFile.CreateDirectoryTree(*SaveDirectory)) {
		FString AbsoluteFilePath = SaveDirectory + "/" + FileName;

		if (AllowOverwriting || !PlatformFile.FileExists(*AbsoluteFilePath)) {
			int32 noiseRand = FMath::Rand();
			TextToSave = FString::FromInt(noiseRand);

			FFileHelper::SaveStringToFile(TextToSave, *AbsoluteFilePath);
		}
	}
	/*TArray<int32> noise;

	for (int NY = 0; NY < ChunkDimenY; NY++) {
		for (int NX = 0; NX < ChunkDimenX; NX++) {
			noise.Add(SimplexNoiseS::SimplexNoise2D(((ChunkXIndex * ChunkElements) + NX) * xMult, ((ChunkYIndex * ChunkElements) + NY) * yMult) * FreqHead);
		}
	}*/

	for (int x = -4; x < 4; x++) {
		for (int y = -4; y < 4; y++) {
			AChunk* achunk = GetWorld()->SpawnActor<AChunk>(ChunkClass, FTransform(FRotator(0.0f, 0.0f, 0.0f), FVector(x * (16 * 100), y * (16 * 100), 0.0f)));

			if (achunk) {
				ChunksArray.Add(achunk);
			}

			/*for (int z = 0; z < 8; z++) {

				int32 indexEwan = x + (y * ChunkElements) + (z * ChunkElements);

				FVector2D ChunkLocation(x * BlockSize, y * BlockSize);

				if (z <	(8 + noise[x + y * ChunkElements])) {
					FTransform localTransform(FRotator(0.0f, 0.0f, 0.0f), FVector(ChunkLocation, z * BlockSize));

					if (z == 7) {
						ABlockRock* spawnedChunk;
						spawnedChunk = GetWorld()->SpawnActor<ABlockRock>(BlockRockClass, FVector(ChunkLocation, z * BlockSize), Rotation, SpawnInfo);
					} else {
						ABlockRock* spawnedChunk;
						spawnedChunk = GetWorld()->SpawnActor<ABlockRock>(BlockRockClass, FVector(ChunkLocation, z * BlockSize), Rotation, SpawnInfo);
					}

					ChunksCollection.Add(FVector2D(x, y));
				}
			}*/
		}
	}	
}

void AMyProjectCharacter::RenderWorld()
{
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	int32 ChunkCoordX = FMath::FloorToInt(GetActorLocation().X / (ChunkSize / 2));
	int32 ChunkCoordY = FMath::FloorToInt(GetActorLocation().Y / (ChunkSize / 2));

	// Remove

	//for (auto& chunkXY : Chunks) {
	//	FVector difRemove = FVector(((chunkXY.X * VoxelSize) + (VoxelSize / 2)), ((chunkXY.Y * VoxelSize) + (VoxelSize / 2)), 0) - FVector(GetActorLocation().X, GetActorLocation().Y, 0);
	//	float renderLengthRemove = difRemove.Size();
	//	float somethingRemove = (VoxelSize * RenderDist);

	//	if (renderLengthRemove >= somethingRemove) {
	//			FVector ChunkLocation3(chunkXY.X * VoxelSize, chunkXY.Y * VoxelSize, 0);
	//			FVector2D trialLocation(chunkXY.X * VoxelSize, chunkXY.Y * VoxelSize);

	//			AChunk* actorToBeRemoved = *(ChunksArray.FindByPredicate([ChunkLocation3](const AChunk* achunk)
	//			{
	//				return achunk->GetActorLocation().Equals(ChunkLocation3);
	//			}));

	//			if (actorToBeRemoved) {
	//				GetWorld()->DestroyActor(actorToBeRemoved);
	//				Chunks.IndexOfByPredicate([ChunkLocation3](FVector2D ) {});
	//			}
	//				
	//				// Destroy Actor


	//			// Remove index of chunks and array
	//	}
	//}

	// Add
	for (int x = -RenderDist; x < RenderDist; x++) {
		for (int y = -RenderDist; y < RenderDist; y++) {
			FVector2D ChunkLocator(FVector2D(x + ChunkCoordX, y + ChunkCoordY));

			FVector dif = FVector(((ChunkLocator.X * ChunkSize) + (ChunkSize /2)), ((ChunkLocator.Y * ChunkSize) + (ChunkSize / 2)), 0) - FVector(GetActorLocation().X, GetActorLocation().Y, 0);
			float renderLength = dif.Size();
			float something = (ChunkSize * RenderDist);

			if (renderLength < something) {
				FString dataAboutCoord2 = FString("X: ") + FString::FromInt(x) + FString("; Y: ") + FString::FromInt(y);
				FString radiusLength = FString("Length: ") + FString::SanitizeFloat(renderLength);

				if (GEngine) {
					GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Yellow, dataAboutCoord2);
					GEngine->AddOnScreenDebugMessage(1, 5.0f, FColor::Red, radiusLength);

				}


				if (!ChunksCollection.Contains(ChunkLocator)) {
					FVector ChunkLocation(ChunkLocator.X * ChunkSize, ChunkLocator.Y * ChunkSize, 0);

					ChunksCollection.Add(ChunkLocator);
					//ChunksArray.Add(GetWorld()->SpawnActor<AChunk>(ChunkClass, ChunkLocation, Rotation, SpawnInfo));
				}
			}
		}
	}
}

void AMyProjectCharacter::BlockCreate()
{
	FHitResult* HitResult = new FHitResult();
	FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
	FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
	FVector EndTrace = ((ForwardVector * 700.0f) + StartTrace);
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();

	bool IsAdding = false;

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *TraceParams))
	{
		DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor(255, 0, 0), true);
		FVector2D ChunkCoord(FMath::FloorToInt(HitResult->ImpactPoint.X / 1600) * 1600, FMath::FloorToInt(HitResult->ImpactPoint.Y / 1600) * 1600);

		for (int32 x = 0; x < ChunksArray.Num(); x++) {
			AChunk* achunk = ChunksArray[x];

			if (achunk->GetActorLocation().X == ChunkCoord.X && achunk->GetActorLocation().Y == ChunkCoord.Y) {

				FVector voxelVector = FVector(0.5f, 0.5f, 0.5f) * 100;
				FVector UnitDirection = (StartTrace - HitResult->Location).GetSafeNormal() * 1;
				
				int32 PoX = FMath::FloorToInt((FMath::Fmod(FMath::Abs(HitResult->Location.X), 1600) + UnitDirection.X + voxelVector.X) / 100);
				int32 PoY = FMath::FloorToInt((FMath::Fmod(FMath::Abs(HitResult->Location.Y), 1600) + UnitDirection.Y + voxelVector.Y) / 100);
				int32 PoZ = FMath::FloorToInt((HitResult->Location.Z + UnitDirection.Z + voxelVector.Z) / 100);

				int32 PoIndex = (PoX * 30 * 16) + (PoY * 30) + PoZ;

				if (achunk->chunkFields.IsValidIndex(PoIndex)) {
					achunk->chunkFields[PoIndex] = 1;
					achunk->RenderChunk();
				}					
			}
		}
	}
}

void AMyProjectCharacter::BlockDestroy()
{
	FHitResult* HitResult = new FHitResult();
	FVector StartTrace = FirstPersonCameraComponent->GetComponentLocation();
	FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
	FVector EndTrace = ((ForwardVector * 700.0f) + StartTrace);
	FCollisionQueryParams* TraceParams = new FCollisionQueryParams();

	bool IsAdding = false;

	if (GetWorld()->LineTraceSingleByChannel(*HitResult, StartTrace, EndTrace, ECC_Visibility, *TraceParams))
	{
		DrawDebugLine(GetWorld(), StartTrace, EndTrace, FColor(255, 0, 0), true);
		FVector2D ChunkCoord(FMath::FloorToInt(HitResult->ImpactPoint.X / 1600) * 1600, FMath::FloorToInt(HitResult->ImpactPoint.Y / 1600) * 1600);

		for (int32 x = 0; x < ChunksArray.Num(); x++) {
			AChunk* achunk = ChunksArray[x];

			if (achunk->GetActorLocation().X == ChunkCoord.X && achunk->GetActorLocation().Y == ChunkCoord.Y) {

				FVector voxelVector = FVector(0.5f, 0.5f, 0.5f) * 100;
				FVector UnitDirection = (StartTrace - HitResult->Location).GetSafeNormal() * -1;

				int32 PoX = FMath::FloorToInt((FMath::Fmod(FMath::Abs(HitResult->Location.X), 1600) + UnitDirection.X + voxelVector.X) / 100);
				int32 PoY = FMath::FloorToInt((FMath::Fmod(FMath::Abs(HitResult->Location.Y), 1600) + UnitDirection.Y + voxelVector.Y) / 100);
				int32 PoZ = FMath::FloorToInt((HitResult->Location.Z + UnitDirection.Z + voxelVector.Z) / 100);

				int32 PoIndex = (PoX * 30 * 16) + (PoY * 30) + PoZ;

				if (achunk->chunkFields.IsValidIndex(PoIndex)) {
					achunk->chunkFields[PoIndex] = 0;
					achunk->RenderChunk();
				}
			}
		}
	}
}

void AMyProjectCharacter::OnFire()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			if (bUsingMotionControllers)
			{
				const FRotator SpawnRotation = VR_MuzzleLocation->GetComponentRotation();
				const FVector SpawnLocation = VR_MuzzleLocation->GetComponentLocation();
				World->SpawnActor<AMyProjectProjectile>(ProjectileClass, SpawnLocation, SpawnRotation);
			}
			else
			{
				const FRotator SpawnRotation = GetControlRotation();
				// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
				const FVector SpawnLocation = ((FP_MuzzleLocation != nullptr) ? FP_MuzzleLocation->GetComponentLocation() : GetActorLocation()) + SpawnRotation.RotateVector(GunOffset);

				//Set Spawn Collision Handling Override
				FActorSpawnParameters ActorSpawnParams;
				ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

				// spawn the projectile at the muzzle
				World->SpawnActor<AMyProjectProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
			}
		}
	}

	// try and play the sound if specified
	if (FireSound != NULL)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, GetActorLocation());
	}

	// try and play a firing animation if specified
	if (FireAnimation != NULL)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Mesh1P->GetAnimInstance();
		if (AnimInstance != NULL)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void AMyProjectCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AMyProjectCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void AMyProjectCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		OnFire();
	}
	TouchItem.bIsPressed = false;
}

//Commenting this section out to be consistent with FPS BP template.
//This allows the user to turn without using the right virtual joystick

//void AMyProjectCharacter::TouchUpdate(const ETouchIndex::Type FingerIndex, const FVector Location)
//{
//	if ((TouchItem.bIsPressed == true) && (TouchItem.FingerIndex == FingerIndex))
//	{
//		if (TouchItem.bIsPressed)
//		{
//			if (GetWorld() != nullptr)
//			{
//				UGameViewportClient* ViewportClient = GetWorld()->GetGameViewport();
//				if (ViewportClient != nullptr)
//				{
//					FVector MoveDelta = Location - TouchItem.Location;
//					FVector2D ScreenSize;
//					ViewportClient->GetViewportSize(ScreenSize);
//					FVector2D ScaledDelta = FVector2D(MoveDelta.X, MoveDelta.Y) / ScreenSize;
//					if (FMath::Abs(ScaledDelta.X) >= 4.0 / ScreenSize.X)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.X * BaseTurnRate;
//						AddControllerYawInput(Value);
//					}
//					if (FMath::Abs(ScaledDelta.Y) >= 4.0 / ScreenSize.Y)
//					{
//						TouchItem.bMoved = true;
//						float Value = ScaledDelta.Y * BaseTurnRate;
//						AddControllerPitchInput(Value);
//					}
//					TouchItem.Location = Location;
//				}
//				TouchItem.Location = Location;
//			}
//		}
//	}
//}

void AMyProjectCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
		//RenderWorld();
	}
}

void AMyProjectCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
		//RenderWorld();
	}
}

void AMyProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMyProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool AMyProjectCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	bool bResult = false;
	if (FPlatformMisc::GetUseVirtualJoysticks() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		bResult = true;
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AMyProjectCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &AMyProjectCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AMyProjectCharacter::TouchUpdate);
	}
	return bResult;
}
