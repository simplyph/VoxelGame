// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ProjectDataSingleton.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class UProjectDataSingleton : public UObject
{
	GENERATED_BODY()
	
public:
	UProjectDataSingleton(const FObjectInitializer& ObjectInitializer);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Project Data Singleton")
		int32 chunkXSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Project Data Singleton")
		int32 chunkYSize;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Project Data Singleton")
		int32 chunkZSize;
	
	
};
