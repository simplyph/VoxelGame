// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Public/SimplexNoiseS.h"
#include "ProceduralMeshComponent.h"
#include "Chunk.generated.h"

struct IMeshSection
{
	TArray<FVector> Vertices;
	TArray<int32> Triangles;
	TArray<FVector> Normals;
	TArray<FVector2D> UVs;
	TArray<FLinearColor> VertexColors;
	TArray<FProcMeshTangent> Tangents;
	int32 elementID = 0;
};

UCLASS()
class MYPROJECT_API AChunk : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChunk();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
		UProceduralMeshComponent* UMesh;

	UPROPERTY(VisibleAnywhere, Category = Mesh)
		USceneComponent* SceneComp;

	UMaterialInterface* MaterialGrass;


	UMaterialInterface* MaterialRock;

	void GenerateNoise();

	void RenderChunk();

	void UpdateChunk();

	UPROPERTY(VisibleAnywhere, Category = "CHUNKIES")
		float CHUNKX;

	UPROPERTY(VisibleAnywhere, Category = "CHUNKIES")
		float CHUNKY;

		TArray<int32> noise;

		TArray<int32> chunkFields;

		TArray<UMaterialInterface*> materials;
};
